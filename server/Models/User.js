const Sequelize = require('sequelize');
const db = require('../Config/dbConfig');

module.exports = db.sequelize.define(
    'user',
    {
        nom: {
            type: Sequelize.STRING,
        },
        prenom: {
            type: Sequelize.STRING
        },
        telephone: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        login: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        lvl: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        id_classe: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
    }
)
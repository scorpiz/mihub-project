const express = require('express');
const router = express.Router();
const cors = require('cors');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

var User = require('../Models/User');

router.use(cors());

process.env.SECRET_KEY = 'secret'

router.get('/get-users', (res, req) => {
    User.findAll().then(users => {
        console.log("all users:", JSON.stringify(users, null, 4));
    })
})

/** Register */
router.post('/register:numRole', (req, res) => {
    let today = new Date()
    let userData = {
        nom: req.body.nom,
        prenom: req.body.prenom,
        telephone: req.body.telephone,
        email: req.body.email,
        // login: ,
        // password: ,
        lvl: req.params.numRole,
        id_classe: req.body.classe
    }

 
    User.findOne({
        where: {
            email: req.body.email
        }
    })
    .then(user => {
        if(!user) {
            const hash = bcrypt.hashSync(userData.password, 10)
            userData.password = hash
            User.create(userData)
            .then(user => {
                let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                    expiresIn: 1440
                })
                res.json({ token: token })
            })
            .catch(err => {
                res.send('error: ' + err)
            })
        } else {
            res.json({ error: 'User already exists' })
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

/** Login */
router.post('/login', (req, res) => {
    User.findOne({
        where: {
            email: req.body.email
        }
    })
    .then(user => {
        if(bcrypt.compareSync(req.body.password, user.password)) {
            let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                expiresIn: 1440
            })
            res.json({ token: token })
        } else {
            res.send('User does not exist')
        }
    })
    .catch(err => {
        res.send('error:' + err)
    })
})

/** Profile */
router.get('/profile', (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

    Users.findOne({
        where: {
            id: decoded.id
        }
    })
    .then(user => {
        if(user) {
            res.json(user)
        } else {
            res.
            send('User does not exist')
        }
    })
    .catch(err => {
        res.send('error:' + err)
    })
})

module.exports = router;

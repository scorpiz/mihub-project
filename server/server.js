const express = require('express');
var app = express();
const bodyParser = require('body-parser');

const userRoute = require('./Routes/User');

app.use(bodyParser.json());

app.use('/users', userRoute);

app.listen(3000, () => {
    console.log('Server is listening on port 3000');
})


